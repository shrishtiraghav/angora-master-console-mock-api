import React from "react";

//Styles
import "./input-field.styles.scss";

const InputField = ({ label, input1, input2, setShowAddContactPersonModal }) => {
  return (
    <div className="input-group mb-20">
      <label htmlFor="author">{label}</label>
      <div className="customer__input-items">
        <input
          type="email"
          className="input-xxl customer__input"
          id="contact-email"
        />
        <i className={`fas fa-plus customer__input-icon`} onClick={() => setShowAddContactPersonModal(true)}></i>
      </div>
      <div className="customer__input-item-container">
        <div className="customer__input-item">
          <div>{input1}</div>
          <div>contact</div>
          <div>email</div>
          <i className={`fas fa-trash customer__input-item-icon`}></i>
        </div>
        <div className="customer__input-item">
          <div>{input2}</div>
          <i className={`fas fa-trash customer__input-item-icon`}></i>
        </div>
      </div>
    </div>
  );
};

export default InputField;
