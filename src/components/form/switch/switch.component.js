import React, { useState } from "react";
import classnames from "classnames";

// Styles
import "./switch.styles.scss";

const Switch = () => {
  const [boolean, setBoolean] = useState(true);

  const handleToggleOption = () => {
    setBoolean(!boolean);
  };

  return (
    <div className="switch__wrapper">
      <div className="switch__container">
        <div
          className={classnames("switch__option switch__option--bg-yes", {
            "switch__option--yes": boolean,
          })}
          onClick={handleToggleOption}
        >
          Yes
        </div>
        <div
          className="switch__option switch__option--white"
          onClick={handleToggleOption}
        ></div>
        <div
          className={classnames("switch__option switch__option--bg-no", {
            "switch__option--no": !boolean,
          })}
          onClick={handleToggleOption}
        >
          No
        </div>
      </div>
    </div>
  );
};

export default Switch;
