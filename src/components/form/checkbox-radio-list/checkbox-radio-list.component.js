import React from "react";
import ReactTooltip from "react-tooltip";

// Styles
import "./checkbox-radio-list.styles.scss";

function CheckboxList({
  options,
  selectedOptions = [],
  labelCol1 = "",
  labelCol2 = "",
  type = "checkbox",
  textColor = "text-grey-900",
  bgColor = "bg-white-00",
  borderColor = "bdr-grey-200"
}) {
  return (
    <div
      className={`checkbox-radio-list ${borderColor} ${bgColor} ${textColor}`}
    >
      <ReactTooltip />
      {/* Search */}
      <div className="checkbox-radio-list__search mb-15">
        <input type="search" name="" placeholder="Filter" autoFocus />
      </div>

      <div className="row">
        {/* Left Col */}
        <div className="row__left--col">
          <h6 className="mb-10">{labelCol1}</h6>

          {options.map(option => (
            <div key={option.value} className="checkbox-radio-list__item">
              <label htmlFor={option.value} className="inline text-14">
                <input
                  type={type}
                  name={option.value}
                  id={option.value}
                  className="mr-7"
                />
                {option.label}
              </label>
            </div>
          ))}
        </div>

        {/* Right Col */}
        <div className="row__right--col">
          <h6 className="mb-10">{labelCol2}</h6>
          {selectedOptions.map(option => (
            <div key={option.value} className="checkbox-radio-list__item">
              <label htmlFor={option.value} className="inline text-14">
                <i data-tip="Remove Item" className="fas fa-times mr-7 cp"></i>
                {option.label}
              </label>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default CheckboxList;
