import React, { useState } from "react";

// Styles
import "./dropdown-search.styles.scss";

const DropDownSearch = ({
  title,
  options,
  onSelect,
  textColor = "text-grey-900",
  bgColor = "bg-white-200",
  borderColor = "bdr-grey-200",
  searchBgColor = "bg-grey-100"
}) => {
  const [showMenu, toggleShowMenu] = useState(false);

  const handleSelect = option => {
    onSelect(option);
    toggleShowMenu(false);
  };

  return (
    <div className="">
      <div
        className={`dropdown__search--wrapper ${textColor} ${bgColor} ${borderColor}`}
      >
        <div
          className="dropdown__search--select cp"
          onClick={() => toggleShowMenu(!showMenu)}
        >
          {title}
          <i className="fas fa-caret-down"></i>
        </div>

        {/* Options */}
        {showMenu ? (
          <div className="dropdown__search--options">
            <div className={`dropdown__search--search-box ${searchBgColor}`}>
              <input type="text" name="" placeholder="Search" autoFocus />
              <i className="fas fa-search dropdown__search--icon"></i>
            </div>

            {/* Option Items */}
            <ul className="dropdown__search--option">
              {options.map(option => (
                <li key={option.id} onClick={() => handleSelect(option)}>
                  {option.label}
                </li>
              ))}
            </ul>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default DropDownSearch;
