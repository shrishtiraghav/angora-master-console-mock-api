import React from "react";

// Styles
import "./dropdown-selected-item.styles.scss"

function DropdownSelectedItem({ selectedOptions }) {
  return (
    <>
      {/* Show selected departments */}
      {selectedOptions.length
        ? selectedOptions.map((option, idx) => (
            <div key={idx} className="dropdown__search--selected-wrapper mt-10">
              <span className="dropdown__search--selected-title">
                {option.label}
              </span>
              <select
                className="dropdown__search--selected-relationship input-md"
                id=""
              >
                <option value="">Admin</option>
                <option value="">Manager</option>
              </select>
              <i className="fas fa-times-circle cp"></i>
            </div>
          ))
        : null}
    </>
  );
}

export default DropdownSelectedItem;
