import React, { useEffect } from "react";
import classnames from "classnames";
import ReactTooltip from "react-tooltip";

// Styles
import "./search-collapsable.styles.scss";

const SearchCollapsable = ({
  showSearch,
  setShowSearch,
  customDataTip = "Search",
  placeholder = "Type to search..."
}) => {
  // Add eventlistener to close the dropdown
  useEffect(() => {
    const handleCloseComponent = e => {
      const TARGET_CLASSNAME = "app__data--search";
      if (!e.target.className.includes(TARGET_CLASSNAME)) {
        setShowSearch(false);
      }
    };

    window.document.addEventListener("click", handleCloseComponent);

    return () =>
      window.document.removeEventListener("click", handleCloseComponent);
  }, [setShowSearch]);

  return (
    <div className="app__data--search-wrapper">
      <ReactTooltip />
      <i
        className={classnames(
          "fas fa-search cp app__data--search-icon-primary",
          {
            hidden: showSearch
          }
        )}
        data-tip={customDataTip}
        onClick={() => setShowSearch(!showSearch)}
      ></i>

      <div
        className={classnames("app__data--search-input-wrapper", {
          "app__data--search-input-wrapper-show": showSearch
        })}
      >
        <i className="fas fa-search app__data--search-icon"></i>
        <input
          type="search"
          className="app__data--search-input"
          placeholder={placeholder}
          onKeyDown={e => (e.which === 27 ? setShowSearch(false) : null)}
        />
      </div>
    </div>
  );
};

export default SearchCollapsable;
