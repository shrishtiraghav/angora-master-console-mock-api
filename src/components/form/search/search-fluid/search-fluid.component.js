import React from "react";

// Styles
import "./search-fluid.styles.scss";

const SearchFluid = ({ placeholder = "Search..." }) => {
  return (
    <div className="search__fluid--wrapper">
      <i className="fas fa-search"></i>
      <input type="search" className="input-xxl" placeholder={placeholder} />
    </div>
  );
};

export default SearchFluid;
