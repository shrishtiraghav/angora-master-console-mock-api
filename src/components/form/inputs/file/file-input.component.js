import React, { useState } from "react";

const InputFile = ({
  btnText = "Select File",
  btnClass = "btn--info btn--lg",
  accept = "*/*",
}) => {
  const [selectedFile, setSelectedFile] = useState("");
  const hiddenFileInput = React.useRef(null);

  const handleClick = () => {
    hiddenFileInput.current.click();
  };

  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    setSelectedFile(fileUploaded);
  };

  return (
    <>
      <input
        type="file"
        accept={accept}
        ref={hiddenFileInput}
        onChange={handleChange}
        style={{ display: "none" }}
      />
      <button className={`btn ${btnClass}`} onClick={handleClick}>
        {selectedFile ? selectedFile.name : btnText}
      </button>
    </>
  );
};

export default InputFile;
