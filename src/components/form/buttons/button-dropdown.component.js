import React, { useState, useEffect } from "react";

import ButtonDropDownMenu from "./button-dropdown-menu/button-dropdown-menu.component";

const ButtonDropDown = ({ icon, color, label, options, onSelected }) => {
  const [visible, setVisible] = useState();

  // Add eventlistener to close the dropdown
  useEffect(() => {
    const handleCloseComponent = (e) => {
      const TARGET_CLASSNAME = "btn--dropdown";
      if (!e.target.className.includes(TARGET_CLASSNAME)) {
        setVisible(false);
      }
    };

    window.document.addEventListener("click", handleCloseComponent);

    return () =>
      window.document.removeEventListener("click", handleCloseComponent);
  }, []);

  const handleOnSelected = (value) => {
    onSelected(value);
    setVisible(false);
  };

  return (
    <div className="btn--dropdown-wrapper">
      <div className="btn--dropdown-container">
        <button
          className={`btn--dropdown-form btn btn--${color} btn--icon`}
          onClick={() => handleOnSelected(label)}
        >
          <i className={`btn--dropdown-form ${icon} btn--icon`}></i>
          {label}
        </button>

        <div
          className={`btn--dropdown btn--dropdown-${color} cp`}
          onClick={() => setVisible(!visible)}
        >
          <i className="btn--dropdown-form fas fa-caret-down"></i>
        </div>
      </div>
      {visible && (
        <ButtonDropDownMenu options={options} onSelected={handleOnSelected} />
      )}
    </div>
  );
};

export default ButtonDropDown;
