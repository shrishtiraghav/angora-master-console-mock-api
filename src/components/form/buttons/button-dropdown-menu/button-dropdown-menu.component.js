import React from "react";
import { Link } from "react-router-dom";

// Styles
import "./button-dropdown-menu.styles.scss";

const ButtonDropDownMenu = ({ options, onSelected }) => {
  return (
    <div className="btn__dropdown--menu-container">
      <ul className="btn__dropdown--menu-items">
        {options &&
          options.map((option, idx) => (
            <li key={idx}>
              <Link to="#" onClick={() => onSelected(option)}>
                {option}
              </Link>
            </li>
          ))}
      </ul>
    </div>
  );
};

export default ButtonDropDownMenu;
