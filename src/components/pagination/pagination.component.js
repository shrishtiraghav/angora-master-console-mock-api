import React from "react";
import { Link } from "react-router-dom";

// Styles
import "./pagination.styles.scss";

const Pagination = () => {
  return (
    <div className="app__pagination mt-30">
      <div className="app__pagination--wrapper">
        <Link className="app__pagination--item" to="/">
          <i className="fas fa-angle-double-left"></i>
        </Link>
        <Link className="app__pagination--item" to="/">
          <i className="fas fa-angle-left"></i>
        </Link>

        <Link className="app__pagination--item" to="/">
          1
        </Link>
        <Link className="app__pagination--item" to="/">
          2
        </Link>
        <Link
          className="app__pagination--item app__pagination--item-active"
          to="/"
        >
          3
        </Link>
        <Link className="app__pagination--item" to="/">
          4
        </Link>
        <Link className="app__pagination--item" to="/">
          5
        </Link>

        <Link className="app__pagination--item" to="/">
          <i className="fas fa-angle-right"></i>
        </Link>
        <Link className="app__pagination--item" to="/">
          <i className="fas fa-angle-double-right"></i>
        </Link>
      </div>
    </div>
  );
};

export default Pagination;
