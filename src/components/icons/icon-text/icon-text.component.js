import React from "react";

// Styles
import "./icon-text.styles.scss";

const IconText = ({ label }) => {
  const getClass = () => {
    const classes = [
      "bg-info text-white-100",
      "bg-primary text-white-100",
      "bg-success text-white-100",
      "bg-danger text-white-100",
      "bg-dark text-white-100",
      "bg-grey text-dark",
      "bg-warning text-white-100",
      "bg-green-sea text-white-100",
      "bg-orange text-white-100",
      "bg-carrot text-white-100",
      "bg-pumpkin text-white-100",
      "bg-indigo text-white-100",
      "bg-river text-white-100",
      "bg-belize text-white-100",
      "bg-wisteria text-white-100",
      "bg-mazrine text-white-100"
    ];

    const random = Math.floor(Math.random() * classes.length);
    return classes[random];
  };

  return (
    <div className={`text__icon ${getClass()}`}>{label[0].toUpperCase()}</div>
  );
};

export default IconText;
