import React from "react";
import classnames from "classnames";

// Styles
import "./icon-folder.styles.scss";

const IconFolder = ({ open, label, selected = false }) => {
  return (
    <div>
      {/* Open Folder */}
      {open ? (
        <div className="icon__folder--wrapper cp">
          <i className="fas fa-caret-down icon__folder--caret mr-5"></i>
          <i className="fas fa-folder-open text-md icon__folder"></i>
          <span
            className={classnames("text-black ml-5", {
              "text-info": selected,
            })}
          >
            {selected ? <strong>{label}</strong> : label}
          </span>
        </div>
      ) : (
        // Close Folder
        <div className="icon__folder--wrapper cp">
          <i className="fas fa-caret-right icon__folder--caret mr-5"></i>
          <i className="fas fa-folder text-md icon__folder"></i>
          <span className="text-black ml-5">{label}</span>
        </div>
      )}
    </div>
  );
};

export default IconFolder;
