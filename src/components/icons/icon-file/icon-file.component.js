import React, { useEffect, useState } from "react";

// Styles
import "./icon-file.styles.scss";

const IconFile = ({ filename, size = "lg", showBackground = false }) => {
  const [color, setColor] = useState("");
  const [icon, setIcon] = useState("");
  const getIcon = () => {
    let extension = filename ? filename.split(".") : "";

    if (extension.length === 1) {
      setColor("dark");
      setIcon("fas fa-folder");
      return;
    }

    extension = extension[extension.length - 1];
    switch (extension) {
      case "png":
      case "jpg":
      case "jpeg":
      case "tif":
      case "tiff":
      case "gif":
        setColor("wisteria");
        setIcon("fas fa-file-image");
        break;

      case "pdf":
        setColor("danger");
        setIcon("fas fa-file-pdf");
        break;

      case "doc":
      case "docx":
        setColor("info");
        setIcon("fas fa-file-word");
        break;

      case "ppt":
      case "pptx":
        setColor("primary");
        setIcon("fas fa-file-powerpoint");
        break;

      case "xls":
      case "xlsx":
        setColor("success");
        setIcon("fas fa-file-excel");
        break;

      case "mp3":
        setColor("carrot");
        setIcon("fas fa-file-audio");
        break;

      case "avi":
      case "mp4":
      case "mpeg":
        setColor("mazrine");
        setIcon("fas fa-file-video");
        break;

      case "zip":
      case "rar":
      case "7zip":
        setColor("warning");
        setIcon("fas fa-file-archive");
        break;

      case "csv":
        setColor("dark");
        setIcon("fas fa-file-csv");
        break;

      case "txt":
        setColor("green-sea");
        setIcon("fas fa-file-alt");
        break;

      default:
        setColor("muted");
        setIcon("fas fa-file");
        break;
    }
  };

  useEffect(() => {
    getIcon();

    // eslint-disable-next-line
  }, []);

  return (
    <span
      className={
        showBackground ? `icon__file--background bg-${color}-light` : null
      }
    >
      <i
        className={`${size ? "text-" + size : null} ${icon} text-${color}`}
      ></i>
    </span>
  );
};

export default IconFile;
