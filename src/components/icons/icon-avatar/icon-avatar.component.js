import React, { useState, useEffect } from "react";
import classnames from "classnames";
import ReactTooltip from "react-tooltip";

// Styles
import "./icon-avatar.styles.scss";

const IconAvatar = ({
  source,
  alt = "",
  size = "md",
  flat = false,
  border = false,
  transparent = false,
  classes = "",
  handleOnClick = () => {},
}) => {
  const [isImage, setIsImage] = useState(false);

  useEffect(() => {
    if (source && source.startsWith("http")) {
      setIsImage(true);
    }
  }, [source]);

  const renderImage = () => {
    return (
      <div className="avatar">
        {alt ? <ReactTooltip /> : null}
        <img
          onClick={handleOnClick}
          alt={alt}
          data-tip={alt}
          className={classnames(
            `cp`,
            {
              avatar__flat: flat,
              avatar__border: border,
              "avatar__flat-10": flat === "10",
              "avatar--xs": size === "xs",
              "avatar--sm": size === "sm",
              "avatar--md": size === "md",
              "avatar--lg": size === "lg",
              "avatar--xl": size === "xl",
              "avatar--xxl": size === "xxl",
            },
            `${classes}`
          )}
          src={source}
        />
      </div>
    );
  };

  const renderText = () => {
    const colors = [
      { background: "bg-success", text: "text-white-100" },
      { background: "bg-info", text: "text-white-100" },
      { background: "bg-warning", text: "text-white-100" },
      { background: "bg-danger", text: "text-white-100" },
      { background: "bg-dark", text: "text-white-100" },
      { background: "bg-green-sea", text: "text-white-100" },
      { background: "bg-orange", text: "text-white-100" },
      { background: "bg-carrot", text: "text-white-100" },
      { background: "bg-pumpkin", text: "text-white-100" },
      { background: "bg-indigo", text: "text-white-100" },
      { background: "bg-river", text: "text-white-100" },
      { background: "bg-belize", text: "text-white-100" },
      { background: "bg-wisteria", text: "text-white-100" },
      { background: "bg-mazrine", text: "text-white-100" },
      { background: "bg-grey", text: "text-dark" },
    ];

    const randomColor = Math.floor(Math.random() * colors.length);

    const chunks = source ? source.split(" ") : [];
    let initials = "";
    for (const chunk of chunks) {
      initials += chunk[0].toUpperCase();
    }

    return (
      <div
        onClick={handleOnClick}
        title={alt}
        className={classnames(
          `avatar cp p5 ${colors[randomColor].background} ${colors[randomColor].text}`,
          {
            avatar__flat: flat,
            avatar__border: border,
            avatar__transparent: transparent,
            "avatar__flat-10": flat === "10",
            "avatar--xs": size === "xs",
            "avatar--sm": size === "sm",
            "avatar--md": size === "md",
            "avatar--lg": size === "lg",
            "avatar--xl": size === "xl",
            "avatar--xxl": size === "xxl",
          },
          `${classes}`
        )}
      >
        {initials}
      </div>
    );
  };

  return <>{isImage ? renderImage() : renderText()}</>;
};

export default IconAvatar;
