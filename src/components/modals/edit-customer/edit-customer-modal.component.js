import React from "react";

import DrawerSubModal from "../../../components/modals/base/drawer/drawer-sub-modal.component";
import EditCustomerFooter from "../edit-customer/edit-customer-footer.component";

// Styles
import "./edit-customer-modal.styles.scss";

const ModalComments = ({
  showAddSubscriptionAndBillingModal,
  setShowAddSubscriptionAndBillingModal,
}) => {
  return (
    <div>
      <DrawerSubModal.Dialog
        title="Manage Plan & Billing"
        showModal={showAddSubscriptionAndBillingModal}
        setShowModal={setShowAddSubscriptionAndBillingModal}
        styles={{ width: "45vw" }}
      >
        <DrawerSubModal.Body
          Footer={() =>
            EditCustomerFooter(setShowAddSubscriptionAndBillingModal)
          }
        >
          <div className="properties">
            {/* Right Column */}
            <div className="properties__right--col">
              <div className="input-group mb-20">
                <div className="input-group mb-20">
                  <label htmlFor="name">Deployment</label>
                  <div className="input-group mb-20 input-flex">
                <div className="input-flex">
                  <input type="checkbox" className="input-xxl flex-item" id="password" />
                  <label htmlFor="author">Cloud</label>
                </div>
                <div className="input-flex">
                  <input type="checkbox" className="input-xxl flex-item" id="password" />
                  <label htmlFor="author">On Premises</label>
                </div>
                </div>
                </div>
                <div className="input-group mb-20">
                  <label htmlFor="name">Subscription Plan</label>
                  <select className="input-xxl" id="contenttype">
                    <option value="">Free</option>
                    <option value="">Premium</option>
                    <option value="">Enterprise-Cloud</option>
                    <option value="">Enterprise-On Premises</option>
                  </select>
                </div>
                <div className="input-group mb-20">
                  <label htmlFor="contenttype">Currency</label>
                  <select className="input-xxl" id="contenttype">
                    <option value="">Rupees</option>
                    <option value="">Dollar</option>
                  </select>
                </div>
              </div>
              <div className="input-group mb-20">
                <label htmlFor="dob">Pricing and Subscription Periods</label>
                <div className="input-group mb-20 input-flex">
                  <div className="input-items">
                  <label htmlFor="dob">Start Date</label>
                  <input
                    type="date"
                    className="input-xxl"
                    id="License-expiry"
                  />
                  </div>
                  <div  className="input-items">
                  <label htmlFor="dob">End date</label>
                  <input
                    type="date"
                    className="input-xxl"
                    id="License-expiry"
                  />
                  </div>
                  <div  className="input-items">
                  <label htmlFor="dob">Increment</label>
                  <input
                    type="text"
                    className="input-xxl"
                    id="License-expiry"
                  />
                  </div>
                  <div  className="input-items">
                  <label htmlFor="dob">Amount</label>
                  <input
                    type="text"
                    className="input-xxl"
                    id="License-expiry"
                  />
                  </div>
                </div>
              </div>
              <div className="input-group mb-20">
                <label htmlFor="author">Payment Mode</label>
                <div className="input-group mb-20 input-flex">
                <div className="input-flex">
                  <input type="checkbox" className="input-xxl flex-item" id="password" />
                  <label htmlFor="author">Check / Wire Transfer</label>
                </div>
                <div className="input-flex">
                  <input type="checkbox" className="input-xxl flex-item" id="password" />
                  <label htmlFor="author">Credit / Debit Card</label>
                </div>
                </div>
              </div>
              <div className="input-group mb-20">
                <label htmlFor="author">Subscription Status</label>
                <div className="input-group mb-20 input-flex">
                <div className="input-flex">
                  <input type="checkbox" className="input-xxl flex-item" id="password" />
                  <label htmlFor="author">Active</label>
                </div>
                <div className="input-flex">
                  <input type="checkbox" className="input-xxl flex-item" id="password" />
                  <label htmlFor="author">Cancelled</label>
                </div>
                </div>
                
              </div>
              {/* End of Right Col */}
            </div>
          </div>
        </DrawerSubModal.Body>
      </DrawerSubModal.Dialog>
    </div>
  );
};

export default ModalComments;
