import React from "react";

import DrawerSubModal from "../../../../components/modals/base/drawer/drawer-sub-modal.component";
import AddContactPersonFooter from "../add-contact/add-contact-footer.component";

// Styles
import "./add-contact-modal.styles.scss";

const ModalComments = ({
  showAddContactPersonModal,
  setShowAddContactPersonModal,
}) => {
  return (
    <div>
      <DrawerSubModal.Dialog
        title="Add Contact Person"
        showModal={showAddContactPersonModal}
        setShowModal={setShowAddContactPersonModal}
        styles={{ width: "45vw" }}
      >
        <DrawerSubModal.Body
          Footer={() =>
            AddContactPersonFooter(setShowAddContactPersonModal)
          }
        >
          <div className="properties">
            {/* Right Column */}
            <div className="properties__right--col">
              <div className="input-group mb-20">
                <div className="input-group mb-20">
                  <label htmlFor="name">Full Name</label>
                  <input
                    type="text"
                    className="input-xxl"
                    id="License-expiry"
                  />
                </div>
                <div className="input-group mb-20">
                  <label htmlFor="name">Email Address 1</label>
                  <input
                    type="text"
                    className="input-xxl"
                    id="License-expiry"
                  />
                </div>
                <div className="input-group mb-20">
                  <label htmlFor="name">Email Address 2</label>
                  <input
                    type="text"
                    className="input-xxl"
                    id="License-expiry"
                  />
                </div>
                <div className="input-group mb-20">
                  <label htmlFor="name">Phone Number 1</label>
                  <input
                    type="text"
                    className="input-xxl"
                    id="License-expiry"
                  />
                </div>
                <div className="input-group mb-20">
                  <label htmlFor="name">Phone Number 2</label>
                  <input
                    type="text"
                    className="input-xxl"
                    id="License-expiry"
                  />
                </div>
              </div>
              <div className="input-group mb-20">
                <div className="input-group mb-20 input-flex">
                <div className="input-flex">
                  <input type="checkbox" className="input-xxl flex-item" id="password" />
                  <label htmlFor="author">Primary Contact</label>
                </div>
                </div>
                
              </div>
              {/* End of Right Col */}
            </div>
          </div>
        </DrawerSubModal.Body>
      </DrawerSubModal.Dialog>
    </div>
  );
};

export default ModalComments;
