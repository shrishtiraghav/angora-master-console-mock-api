import React from "react";

import DrawerModal from "../../../../components/modals/base/drawer/drawer-modal.component";

const EditCustomerFooter = (setShowAddContactPersonModal) => {
  return (
    <DrawerModal.Footer>
      <div className="row">
        <button className="btn btn--primary btn--xxl mr-10">Save & Exit</button>
      </div>
      <button
        className="btn btn--outline-primary btn--xxl"
        onClick={() => setShowAddContactPersonModal(false)}
      >
        Cancel
      </button>
    </DrawerModal.Footer>
  );
};

export default EditCustomerFooter;