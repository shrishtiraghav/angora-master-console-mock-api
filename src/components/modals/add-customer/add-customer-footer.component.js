import React from "react";

import DrawerModal from "../base/drawer/drawer-modal.component";

const AddCustomerFooter = (setShowAddCustomerModal,setShowAddSubscriptionAndBillingModal,postCustomers,dispatch,companyFullLegalName,companyDisplayName) => {
  return (
    <DrawerModal.Footer>
      <div className="row">
        <button className="btn btn--primary btn--xxl mr-10"  onClick={() => setShowAddSubscriptionAndBillingModal(true)}>Add Subscription & Billing Information <i className={`fas fa-angle-double-right`}></i></button>
      </div>
      <button
        className="btn btn--outline-primary btn--xxl"
        onClick={() => {setShowAddCustomerModal(false) 
        dispatch(postCustomers({company_full_legal_name: companyFullLegalName, company_display_name: companyDisplayName}))}}
      >
        Cancel
      </button>
    </DrawerModal.Footer>
  );
};

export default AddCustomerFooter;
