import React from "react";

// Styles
import "./drawer-modal-sub-header.styles.scss";

const Header = ({ title, setShowModal }) => {
  return (
    <div className="app__drawer-modal-sub--header">
        <i
        className="fas fa-arrow-left close--icon mr-10 cp"
        id="newsMenuCloseBtn"
        onClick={() => setShowModal(false)}
      ></i>
      <h5 className="ml-10">{title}</h5>
    </div>
  );
};

export default Header;