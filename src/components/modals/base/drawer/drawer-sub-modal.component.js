import Dialog from "./drawer-modal-sub-dialog/drawer-modal-sub-dialog.component";
import Body from "./drawer-modal-body/drawer-modal-body.component";
import Footer from "./drawer-modal-footer/drawer-modal-footer.component";

const DrawerSubModal = {};

DrawerSubModal.Dialog = Dialog;
DrawerSubModal.Body = Body;
DrawerSubModal.Footer = Footer;

export default DrawerSubModal;