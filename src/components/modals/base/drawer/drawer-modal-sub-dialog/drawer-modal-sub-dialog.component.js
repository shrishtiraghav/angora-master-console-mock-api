import React from "react";
import classnames from "classnames";

import Header from "../drawer-modal-sub-header/drawer-modal-sub-header.component";

// Styles
// import "./";

const drawerModal = ({
  title,
  showModal,
  setShowModal,
  children,
  styles = {},
}) => {
  return (
    <div className="app__drawer-modal--wrapper">
      {showModal}
      <div
        className={classnames("app__drawer-modal--menu", {
          "app__drawer-modal--menu-show": showModal,
        })}
        style={{ ...styles }}
      >
        {/* Header */}
        <Header title={title} setShowModal={setShowModal} />

        {children}
      </div>
    </div>
  );
};

export default drawerModal;
