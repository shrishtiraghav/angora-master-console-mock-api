import React from "react";

// Styles
import "./default-modal.styles.scss";

const BasicModal = ({
  children,
  setShowModal,
  width = "80%",
  height = "auto",
}) => {
  return (
    <div className="app__modal--basic-wrapper">
      <div className="app__modal--basic-container" style={{ width, height }}>
        {/* Close button */}
        <div className="app__modal--basic-close-container">
          <i
            className="fas fa-times app__modal--basic-close cp"
            onClick={() => setShowModal(false)}
          ></i>
        </div>

        {children}
      </div>
    </div>
  );
};

export default BasicModal;
