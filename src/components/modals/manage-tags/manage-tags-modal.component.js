import React from "react";
import PropTypes from "prop-types";

import BasicModal from "components/modals/base/default/default-modal.component";
import OutsideClickHandler from "react-outside-click-handler";

// Styles
import "./manage-tags-modal.styles.scss";

const ManageTagsModal = ({
  showManageTagsModal,
  setManageTagsModal,
  values,
  setValues,
  tagSuggestions,
  showDropdown,
  setShowDropdown,
  handleChange,
  handleSelect,
  handleSubmit
}) => {
  return (
    <div className="manage__tags-modal">
      <BasicModal
        showModal={showManageTagsModal}
        setShowModal={setManageTagsModal}
        width="25vw"
        height="auto"
      >
        {/* title */}
        <h5 className="mb-20">Add Tags</h5>

        {/* input container */}
        <div className="input-group mt-10 w-100">
          <div className="manage__tags-input-container">
            {values.map(value => (
              <div
                key={value.tag}
                className="manage__tags-input-list-item-container"
              >
                <li key={value.tag} className="manage__tags-input-list-item">
                  {value.tag}
                </li>
                <i
                  className={`far fa-times-circle manage__tags-input-list-item-icon`}
                  onClick={() => setValues(values.filter(item => item.tag !== value.tag))}
                ></i>
              </div>
            ))}

            <input
              id="mainInput"
              className="manage__tags-input-field"
              placeholder="Add Tags..."
              onKeyUp={handleChange}
              onChange={() => setShowDropdown(true)}
            ></input>
          </div>

          {/* dropdown */}
          {showDropdown ? (
            <OutsideClickHandler onOutsideClick={() => setShowDropdown(false)}>
              <div className="manage__tags-dropdown-container">
                {tagSuggestions.map(item => (
                  <li
                    key={item.suggestion}
                    className="manage__tags-dropdown-list-item"
                    onClick={() => handleSelect(item.suggestion)}
                  >
                    {item.suggestion}
                  </li>
                ))}
              </div>{" "}
            </OutsideClickHandler>
          ) : null}
        </div>

        {/* button */}
        <div className="input-group mt-10 w-100">
          <button
            className="btn btn--primary btn--xl btn--block"
            onClick={handleSubmit}
          >
            Save
          </button>
        </div>
      </BasicModal>
    </div>
  );
};

ManageTagsModal.propTypes = {
  showManageTagsModal: PropTypes.bool.isRequired,
  setManageTagsModal: PropTypes.func.isRequired,
  values:PropTypes.array.isRequired,
  setValues:PropTypes.func.isRequired,
  tagSuggestions:PropTypes.array.isRequired,
  showDropdown:PropTypes.bool.isRequired,
  setShowDropdown:PropTypes.func.isRequired,
  handleChange:PropTypes.func.isRequired,
  handleSelect:PropTypes.func.isRequired,
  handleSubmit:PropTypes.func.isRequired,
};

export default ManageTagsModal;
