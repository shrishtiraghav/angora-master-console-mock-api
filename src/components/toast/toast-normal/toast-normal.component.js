// @see https://github.com/CodeSeven/toastr
import toastr from "toastr";

// Styles
import "toastr/build/toastr.css";
import "./toast-normal.styles.scss";

toastr.options.progressBar = true;
toastr.options.closeButton = true;

export default toastr;
