import React from "react";

// Styles
import "./toast-full.styles.scss";

const Toast = ({ children, setShowToast }) => {
  return (
    <div className="app__toast app__toast--grey p-7">
      <div className="app__toast--left-col">{children}</div>
      <div className="app__toast--right-col mr-20  cp">
        <i className="fas fa-times" onClick={(e) => setShowToast(false)}></i>
      </div>
    </div>
  );
};

export default Toast;
