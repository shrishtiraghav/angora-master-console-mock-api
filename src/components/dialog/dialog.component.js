// @see https://alertifyjs.com/
import alertify from "alertifyjs";

// Styles
import "alertifyjs/build/css/alertify.min.css";
import "alertifyjs/build/css/themes/default.min.css";
import "./dialog.styles.scss";

export default alertify;
