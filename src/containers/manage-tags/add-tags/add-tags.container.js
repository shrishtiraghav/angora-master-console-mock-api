import React, { useState, useEffect } from "react";

import ManageTags from "../manage-tags.container";

import Button from "components/form/buttons/button/button.component";
import ManageTagsModal from "components/modals/manage-tags/manage-tags-modal.component";

// Hooks
import useHttp from "hooks/useHttp";

// Styles
import "./app-tags.styles.scss";

const SelectedTags = () => {
  const [showManageTagsModal, setManageTagsModal] = useState(false);
  const [values, setValues] = useState([]);
  const [showDropdown, setShowDropdown] = useState(false);
  const [tagSuggestions, setTagSuggestions] = useState([
    { suggestion: "item" },
    { suggestion: "content" },
  ]);
  const {
    get: getTags,
    isLoading: isTagsLoading,
    response: tagsResponse,
    error: tagsError,
  } = useHttp(`/tags`);

  const {
    post: postTags,
    error: postError,
  } = useHttp(`/tags`);

  const {
    deletePost: deleteTags,
    error: deleteError,
  } = useHttp(`/tags`);

  useEffect(() => {
    getTags({});
  }, []);

  const handleChange = (e) => {
    if (e.key === "Enter" && e.target.value.length !== 0) {
      postTags({ name: e.target.value });
      getTags({});
      setValues([...values, { tag: e.target.value }]);
      mainInput.value = "";
      setShowDropdown(false);
    }
  };

  const handleSelect = (tag) => {
    postTags({ name: tag });
    getTags({});
    setValues([...values, { tag: tag }]);
    setShowDropdown(false);
  };

  const handleSubmit = () => {
    setValues([]);
    setManageTagsModal(false);
  };

  const handleDelete = (id) => {
    deleteTags(id)
    getTags({});
  }

  return (
    <ManageTags>
      {showManageTagsModal && (
        <ManageTagsModal
          showManageTagsModal={showManageTagsModal}
          setManageTagsModal={setManageTagsModal}
          values={values}
          setValues={setValues}
          tagSuggestions={tagSuggestions}
          showDropdown={showDropdown}
          setShowDropdown={setShowDropdown}
          handleChange={handleChange}
          handleSelect={handleSelect}
          handleSubmit={handleSubmit}
        />
      )}

      {/*Tags Container */}
      <div className="app__item-tag-items">
        {tagsResponse?.data &&
          tagsResponse.data.tags.map((tag) => (
            <div key={tag.id} className="app__item-tag-item-container">
              <div className="app__item-tag-item">{tag.name}</div>
              <div
                className="app__item-tag-item-icon"
                onClick={()=>handleDelete(tag.id)}
              >
                <i
                  className={`far fa-times-circle manage__tags-input-list-item-icon`}
                ></i>
              </div>
            </div>
          ))}
      </div>

      {/*Button */}
      <div className="app__item-manage-tags-button">
        <Button
          icon="fas fa-pen"
          color="info"
          label={"MANAGE  TAGS"}
          handleButton={() => setManageTagsModal(true)}
        ></Button>
      </div>
    </ManageTags>
  );
};

export default SelectedTags;
