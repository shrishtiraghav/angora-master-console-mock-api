import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import ReactTooltip from "react-tooltip";
import { useDispatch, useSelector } from "react-redux";
import {
  getCustomers,
  getCustomerPlanAndBilling,
  postCustomers,
} from "store/customers/customers.actions";
import * as customers from "store/store";

import DefaultLayout from "layouts/default/default.layout";
import Button from "components/form/buttons/button/button.component";
import SearchCollapsable from "components/form/search/search-collapsable/search-collapsable.component";
import Pagination from "components/pagination/pagination.component";
import DropDown from "components/form/dropdown/dropdown.component";
import DropDownSearch from "components/form/dropdown-search/dropdown-search.component";
import AddCustomerModal from "components/modals/add-customer/add-customer-modal.component";
import AddSubscriptionAndBillingModal from "components/modals/edit-customer/edit-customer-modal.component";
import AddContactPersonModal from "components/modals/add-customer/add-contact/add-contact-modal.component";

// Styles
import "./customers.styles.scss";

export const ADD_CUSTOMER = "Add Customer";
export const SEND_NEWS_NOTIFICATIONS = "Send news notification";

export const EXPORT_CUSTOMERS = "Export Customers";
export const SUSPEND_INSTANCE = "Suspend Instance";
export const TERMINATE_INSTANCE = "Terminate Instance";

const Customer = () => {
  let history = useHistory();
  const [showSearch, setShowSearch] = useState(false);
  const [showAddCustomerModal, setShowAddCustomerModal] = useState(false);
  const [
    showAddSubscriptionAndBillingModal,
    setShowAddSubscriptionAndBillingModal,
  ] = useState(false);
  const [showAddContactPersonModal, setShowAddContactPersonModal] = useState(
    false
  );
  const dispatch = useDispatch();

  const customersData = useSelector((state) => {
    return customers.accessCustomers(state);
  });

  const customersPlanAndBilling = useSelector((state) => {
    return customers.accessCustomerPlanAndBilling(state);
  });

  useEffect(() => {
    dispatch(getCustomers());
    dispatch(getCustomerPlanAndBilling());
  }, [dispatch]);

  const handleButton = (value) => {
    switch (value) {
      case ADD_CUSTOMER:
        setShowAddCustomerModal(true);
        break;
      default:
        break;
    }
  };

  const handleNewsNotifications = () => {
    history.push("/manage-customers/news-notifications");
  };

  const handleFilter = () => {};

  const filterOptons = [
    {
      label: EXPORT_CUSTOMERS,
      value: EXPORT_CUSTOMERS,
    },
    {
      label: SUSPEND_INSTANCE,
      value: SUSPEND_INSTANCE,
    },
    {
      label: TERMINATE_INSTANCE,
      value: TERMINATE_INSTANCE,
    },
  ];

  return (
    <DefaultLayout>
      {
        <AddCustomerModal
          showAddCustomerModal={showAddCustomerModal}
          setShowAddCustomerModal={setShowAddCustomerModal}
          setShowAddSubscriptionAndBillingModal={
            setShowAddSubscriptionAndBillingModal
          }
          setShowAddContactPersonModal={setShowAddContactPersonModal}
          postCustomers={postCustomers}
          dispatch={dispatch}
        />
      }
      {
        <AddSubscriptionAndBillingModal
          showAddSubscriptionAndBillingModal={
            showAddSubscriptionAndBillingModal
          }
          setShowAddSubscriptionAndBillingModal={
            setShowAddSubscriptionAndBillingModal
          }
        />
      }
      {
        <AddContactPersonModal
          showAddContactPersonModal={showAddContactPersonModal}
          setShowAddContactPersonModal={setShowAddContactPersonModal}
        />
      }
      <div className="container">
        {/* Content Header */}

        <div className="app__data--listing-header">
          <div className="app__data--listing-header-top mb-20">
            <div className="app__data--listing-header-left-col">
              <div className="app__data--listing-title">
                <i className={`fas fa-users app__navigation--item-icon`}></i>{" "}
                Manage Customers
              </div>
            </div>
            <div className="app__data--listing-header-right-col">
              {/* Button */}
              <div classname="app__data--listing-header button">
                <Button
                  icon="fas fa-user-plus"
                  color="info"
                  label={ADD_CUSTOMER}
                  handleButton={handleButton}
                ></Button>
              </div>
              {/* Button */}
              <div>
                <Button
                  icon="fas fa-newspaper"
                  color="info"
                  label={SEND_NEWS_NOTIFICATIONS}
                  handleButton={handleNewsNotifications}
                ></Button>
              </div>
            </div>
          </div>

          {/* Filters & Search */}
          <div className="app__data--listing--filter mt-20">
            <div className="app__data--listing--filter-menu">
              <DropDown
                title="Selected Customer(s)"
                options={filterOptons}
                handleFilter={handleFilter}
              />
              <SearchCollapsable
                dataTip="Search Customer(s)"
                showSearch={showSearch}
                setShowSearch={setShowSearch}
                placeholder="Search by username"
              />
            </div>
          </div>
        </div>

        {/* Content Body */}

        <table className="table mt-20">
          <thead>
            <tr>
              <th>
                <input type="checkbox" className="mr-15" id="" />
                Company
              </th>
              <th>
                <div>Primary Contact Name</div>
              </th>
              <th>
                <div>Primary Contact Phone</div>
              </th>
              <th>
                <div>Primary Contact Email</div>
              </th>
              <th>
                <div>Active Status</div>
              </th>
              <th>
                <div>Last Login</div>
              </th>
              <th>
                <div>Expiry Date</div>
              </th>
            </tr>
          </thead>

          <tbody>
            {/* Row */}
            {customersData?.length &&
              customersData.map((customer) => (
                <tr key={customer.id}>
                  <td>
                    <div className="table__col--main">
                      <div className="mr-10">
                        <input type="checkbox" className="mr-15" id="test" />
                      </div>
                      <div className="title-wrapper">
                        <Link to="/manage-customers/1/users">
                          {customer.companyFullLegalName}
                        </Link>
                      </div>
                    </div>
                  </td>
                  <td className="text-muted">
                    {customer.contactPersons.map((contactPerson) =>
                      contactPerson.type === "primary" ? (
                        <td className="text-muted" key={contactPerson.name}>
                          {contactPerson.name}
                        </td>
                      ) : null
                    )}
                  </td>
                  <td className="text-muted">
                    {customer.contactPersons.map((contactPerson) =>
                      contactPerson.type === "primary" ? (
                        <td
                          className="text-muted"
                          key={contactPerson.contactNumber}
                        >
                          {contactPerson.contactNumber}
                        </td>
                      ) : null
                    )}
                  </td>
                  <td className="text-muted">
                    {customer.contactPersons.map((contactPerson) =>
                      contactPerson.type === "primary" ? (
                        <td
                          className="text-muted"
                          key={contactPerson.contactEmail}
                        >
                          {contactPerson.contactEmail}
                        </td>
                      ) : null
                    )}
                  </td>
                  <td className="text-muted">
                    {customersPlanAndBilling?.length &&
                      customersPlanAndBilling.map((customerPlanAndBilling) =>
                        customerPlanAndBilling.id === customer.id ? (
                          <td key={customerPlanAndBilling.id}>
                            {customerPlanAndBilling.subscriptionStatus}
                          </td>
                        ) : null
                      )}
                  </td>
                  <td className="text-muted"></td>
                  <td className="text-muted">{customer.license_expiry}</td>
                  <td>
                    <div className="table__action--button-wrapper">
                      <ul>
                        <li className="table__action--button">
                          <Link to="#">Export Users</Link>
                        </li>
                        <li className="table__action--button-divider">|</li>
                        <li className="table__action--button">
                          <Link
                            to={`/manage-customers/${customer.id}/account-usage-users`}
                          >
                            Account Usage
                          </Link>
                        </li>
                        <li className="table__action--button-divider">|</li>
                        <li
                          className="table__action--button"
                          onClick={() => handleButton(ADD_CUSTOMER)}
                        >
                          <Link to="#">Edit</Link>
                        </li>
                        <li className="table__action--button-divider">|</li>
                        <li className="table__action--button">
                          <Link to={`/manage-customers/${customer.id}/profile`}>
                            Profile
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>

        {/* Pagination */}
        <Pagination />

        {/* End Container */}
      </div>
    </DefaultLayout>
  );
};

export default Customer;
