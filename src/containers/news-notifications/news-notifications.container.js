import React, {useState} from "react";
import { Link } from "react-router-dom";
import ReactMarkdown from "react-markdown";
import MDEditor from '@uiw/react-md-editor';

import DropDown from "components/form/dropdown/dropdown.component";
import DropDownSearch from "components/form/dropdown-search/dropdown-search.component";
import Button from "components/form/buttons/button/button.component";
import DefaultLayout from "layouts/default/default.layout";

// Styles
import "./news-notifications.styles.scss";

const ManageTags = () => {
  const [content , setContent] = useState('')
  const [selectedCustomers , setSelectedCustomers] = useState([])

  const handleContent = (e) => {
    setContent(e.target.value)
  }

  const handleButton = () => {

  }

  const onSelect = (option) => {
    setSelectedCustomers([...selectedCustomers , option])
    console.log(selectedCustomers)
  };

  const filterOptons = [
    {
      label: "AVANTA",
      value: "AVANTA",
    },
    {
      label: "ACME",
      value: "ACME",
    },
    {
      label: "LUMINIOUS",
      value: "LUMINIOUS",
    },
  ];

  return (
    <DefaultLayout>
      <div className="container">
        {/* Content Header */}
        <div className="app__data--listing-header">
          <div className="app__data--listing-header-top mb-20">
            <div className="app__data--listing-header-left-col">
              <div className="app__data--listing-title">
              <Link to="/manage-customers" className="mr-10">
                  <i
                    className="fas fa-long-arrow-alt-left"
                    data-tip="Back to Manage Customers"
                  ></i></Link>
                <i className={`fas fa-newspaper `}></i> Add News Notifications
              </div>
            </div>
          </div>
        </div>

        {/* Content Body */}
        <div className="app__item--account--wrapper mb-20 mt-20">
          <div className="card app__item--news-notification-card">
            <div className="app__item--news-notification--container">
              <div className="app__item--title">Send News Notification To 
              {selectedCustomers?.length &&
              selectedCustomers.map((customer) => (<div className="app__item--customer">{customer.value}</div>))}</div>
              {/* <div className="app__item--description">Compose news using markdown</div> */}
              <div className="app__item--body">
                  <div className="app__item--markdown">
                      <div className="app__item--markdown-heading">Compose news using markdown</div>
                      <div className="app__item--markdown-content"><MDEditor
                        value={content}
                        onChange={setContent}
                      /> </div> 
                      {/* <div className="app__item--markdown-content"><textarea onChange={handleContent} rows='50' columns='10'></textarea></div> */}
                  </div>
                  <div className="app__item--preview">
                    <div className="app__item--preview-heading">Live Preview</div>
                    {/* <div className="app__item--preview-content"><ReactMarkdown children={content} /></div> */}
                    <div className="app__item--preview-content"><MDEditor.Markdown source={content} /></div>
                    </div>
              </div>

              <div className="app__item--link-heading">
                  <div className="app__item--link">Link to detailed article or blog post</div>
              </div>
              <div className="app__item--send">
                  <div className="app__item--send-heading">Send to</div>
                  <div><DropDownSearch title="Select Customer(s)" options={filterOptons} onSelect={onSelect}/></div>
                  {/* <div> <DropDown
                title="Select Customer(s)"
                options={filterOptons}
                handleFilter={handleFilter}
              /></div> */}
                  <div className="app__item--send-button"><Button
                icon="fa fa-paper-plane "
                color="info"
                label={"SEND"}
                handleButton={handleButton}
              ></Button></div>
              <div><Button
                icon="fas fa-times"
                color="primary"
                label={"CANCEL"}
                handleButton={handleButton}
              ></Button></div>
              </div>
            </div>
          </div>
        </div>

        {/* End Container */}
      </div>
    </DefaultLayout>
  );
};

export default ManageTags;
