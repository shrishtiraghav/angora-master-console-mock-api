import React from "react";
import {Link } from "react-router-dom";

import DefaultLayout from "layouts/default/default.layout";
import UserAccountSideBar from "./user-account-sidebar/user-account-sidebar.container";
import UserProfile from "../user-account/user-profile/user-profile.container";
import UserContacts from "../user-account/user-contacts/user-contacts.container";
import UserSubscriptionHistory from "../user-account/user-subscription-history/user-subscription-history.container"

// Styles
import "./user-account.styles.scss";

const UserAccount = ({ location }) => {

  {/* dynamic layouts*/}
  const loadComponent = () => {
    const { pathname } = location;
    if (pathname.search("profile") !== -1) {
      return <UserProfile />;
    } else if (pathname.search("contact-persons") !== -1) {
      return <UserContacts />;
    } else if (pathname.search("subscription-history") !== -1) {
      return <UserSubscriptionHistory />;
    }
  };

  return (
    <DefaultLayout>
      <div className="container">
        {/* Content Header */}
        <div className="app__data--listing-header">
          <div className="app__data--listing-header-top mb-20">
            <div className="app__data--listing-header-left-col">
              <div className="app__data--listing-title">
                <Link to="/manage-customers" className="mr-10">
                  <i
                    className="fas fa-long-arrow-alt-left"
                    data-tip="Back to Manage Customers"
                  ></i>
                </Link>  
                Account
              </div>
            </div>
          </div>
         
        </div>

        {/* Content Body */}
        <div className="app__user--account--wrapper mb-20 mt-20">
          <div className="card app__user--account-card">
            <div className="app__user--account--container">
              {/* Left Col */}
              <div className="app__user--account-left-col">
                <UserAccountSideBar />
              </div>
              {/* Right Col */}
              <div className="app__user--account-right-col">
                {loadComponent()}
              </div>
            </div>
          </div>
        </div>

        {/* End Container */}
      </div>
    </DefaultLayout>
  );
};

export default UserAccount;
