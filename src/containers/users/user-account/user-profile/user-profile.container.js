import React, { useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getCustomerById,
  getCustomerPlanAndBillingById,
} from "store/customers/customers.actions";

import * as customers from "store/store";

// Styles
import "./user-profile.styles.scss";

const Profile = () => {
  let history = useHistory();
  let params = useParams();

  const id = params.id;
  const dispatch = useDispatch();

  const customerData = useSelector((state) => {
    return customers.accessCustomerById(state);
  });

  const customerPlanAndBillingData = useSelector((state) => {
    return customers.accessCustomerPlanAndBillingById(state);
  });

  useEffect(() => {
    dispatch(getCustomerById(id));
    dispatch(getCustomerPlanAndBillingById(id));
  }, [dispatch]);

  return (
    <div>
      <h4 className="app__user--profile-form-heading">Configuration</h4>

      {/*profile info container */}
      <div className="app__user--profile-form">
        <div className="hr mt-20"></div>

        {customerData?.customer &&
          customerPlanAndBillingData?.customerPlanAndBilling && (
            <div>
              <div className="mt-30 col-2">
                <label htmlFor="email" className="text-md mr-20 text-right">
                  DMS URL
                </label>
                <div className="app__user--label-entry">
                  {customerData.customer.customerSubdomain}
                </div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="email" className="text-md mr-20 text-right">
                  DMS Name
                </label>
                <div className="app__user--label-entry">
                  {customerData.customer.companyDisplayName}
                </div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="email" className="text-md mr-20 text-right">
                  Organisation
                </label>
                <div className="app__user--label-entry">
                  {customerData.customer.companyFullLegalName}
                </div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="email" className="text-md mr-20 text-right">
                  Default Email Address
                </label>
                <div className="app__user--label-entry">
                  {customerData.customer.primaryAdminEmail}
                </div>
              </div>

              <div className="mt-30 col-2">
                <label
                  htmlFor="firstname"
                  className="text-md  mr-20 text-right"
                >
                  Server
                </label>
                <div className="app__user--label-entry">
                  {
                    customerPlanAndBillingData.customerPlanAndBilling
                      .deploymentType
                  }
                </div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="lastname" className="text-md  mr-20 text-right">
                  License Validity
                </label>
                <div className="app__user--label-entry">
                  {customerData.customer.licenseExpiry}
                </div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="title" className="text-md  mr-20 text-right">
                  Active Subscription Type
                </label>
                <div className="app__user--label-entry">
                  {
                    customerPlanAndBillingData.customerPlanAndBilling
                      .subscriptionPlan
                  }
                </div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="title" className="text-md  mr-20 text-right">
                  Last Login
                </label>
                <div className="app__user--label-entry"></div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="title" className="text-md  mr-20 text-right">
                  Last Login User
                </label>
                <div className="app__user--label-entry"></div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="title" className="text-md  mr-20 text-right">
                  No. of Users
                </label>
                <div className="app__user--label-entry"></div>
              </div>

              <div className="mt-30 col-2">
                <label htmlFor="title" className="text-md  mr-20 text-right">
                  License Last Updated
                </label>
                <div className="app__user--label-entry"></div>
              </div>
              <div className="mt-30 col-2">
                <label htmlFor="title" className="text-md  mr-20 text-right">
                  Point of Contact
                </label>
                <div className="app__user--label-entry">
                  {customerData.customer.primaryAdminFirstName}{" "}
                  {customerData.customer.primaryAdminLastName}
                </div>
              </div>
            </div>
          )}

        <div className="hr my-30"></div>

        {/* End Form Wrapper */}
      </div>
    </div>
  );
};

export default Profile;
