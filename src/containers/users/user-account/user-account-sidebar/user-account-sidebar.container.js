import React from "react";
import { NavLink, useParams, useHistory } from "react-router-dom";

import IconAvatar from "components/icons/icon-avatar/icon-avatar.component";

// Styles
import "./user-account-sidebar.styles.scss";

const UserAccountSideBar = () => {
  let history = useHistory();
  let params = useParams();

  const id = params.id;

  return (
    <>
      <div className="app__user--account-sidebar-top">
        <IconAvatar
          source="https://randomuser.me/api/portraits/women/49.jpg"
          alt="Jane Doe"
          size="xl"
          classes="avatar__border"
        />

        <div className="app__user--account-sidebar-name mt-10">Customer 1</div>
      </div>

      {/* Bottom Navigations */}
      <div className="app__user--account-sidebar-bottom mt-20">
        <ul>
          <li>
            <NavLink
              to={`/manage-customers/${id}/profile`}
              activeClassName="app__user--account-sidebar-active"
            >
              <i className="fas fa-cog"></i> <span>Configuration</span>
            </NavLink>
          </li>
          <li>
            <NavLink
              to={`/manage-customers/${id}/contact-persons`}
              activeClassName="app__user--account-sidebar-active"
            >
              <i className="fas fa-phone"></i> <span>Contact Persons</span>
            </NavLink>
          </li>
          <li>
            <NavLink
              to={`/manage-customers/${id}/subscription-history`}
              activeClassName="app__user--account-sidebar-active"
            >
              <i className="fas fa-history"></i>{" "}
              <span>Subscription History</span>
            </NavLink>
          </li>
        </ul>
      </div>
    </>
  );
};

export default UserAccountSideBar;
