import React from "react";
import { useSelector } from "react-redux";

import * as customers from "store/store";

const UserSubscriptionHistory = () => {
  const customerPlanAndBillingData = useSelector((state) => {
    return customers.accessCustomerPlanAndBillingById(state);
  });

  return (
    <>
      <h4 className="app__user--profile-form-heading">Subscription History</h4>
      <div className="app__user--profile-form">
        <table className="table mt-20">
          <thead>
            <tr>
              <th>Date</th>
              <th>Plan</th>
              <th>Amount</th>
            </tr>
          </thead>

          <tbody>
            {/* Row */}
            {customerPlanAndBillingData?.customerPlanAndBilling &&
              customerPlanAndBillingData.customerPlanAndBilling.pricingAndSubscriptionPeriods.map(
                (pricingAndSubscriptionPeriod) => (
                  <tr key={pricingAndSubscriptionPeriod.startDate}>
                    <td>{pricingAndSubscriptionPeriod.startDate}</td>
                    <td>
                      {
                        customerPlanAndBillingData.customerPlanAndBilling
                          .subscriptionPlan
                      }
                    </td>
                    <td className="text-muted">
                      {pricingAndSubscriptionPeriod.amount}
                    </td>
                  </tr>
                )
              )}
          </tbody>
        </table>
        <div className="hr my-30"></div>

        {/* End Form Wrapper */}
      </div>
    </>
  );
};

export default UserSubscriptionHistory;
