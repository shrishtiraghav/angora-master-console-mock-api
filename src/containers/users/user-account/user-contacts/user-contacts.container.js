import React from "react";
import { useSelector } from "react-redux";
import * as Customer from "store/store";

const UserContacts = () => {
  const customerData = useSelector((state) => {
    return Customer.accessCustomerById(state);
  });

  return (
    <div>
      <h4 className="app__user--profile-form-heading">Contact Persons</h4>
      <div className="app__user--profile-form">
        {/*table */}
        <table className="table mt-20">
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone Number</th>
              <th>Email</th>
            </tr>
          </thead>
          {/*table body*/}
          {customerData?.customer && (
            <tbody>
              {/* Row */}
              {customerData.customer.contactPersons.map((contactPerson) => (
                <tr key={contactPerson.name}>
                  <td className="text-muted">{contactPerson.name}</td>
                  <td className="text-muted">{contactPerson.contactNumber}</td>
                  <td className="text-muted">{contactPerson.contactEmail}</td>
                </tr>
              ))}
            </tbody>
          )}
        </table>
        <div className="hr my-30"></div>

        {/* End Form Wrapper */}
      </div>
    </div>
  );
};

export default UserContacts;
