import React, { useState } from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";

import LoginLayout from "layouts/login/login.layout";
import logo from "assets/img/logos/logo-md.png";

// Styles
import "../auth.styles.scss";

const Login = ({ history }) => {
  const [showSignIn, setShowSignIn] = useState(true);
  const [showForgotPassword, setShowForgotPassword] = useState(false);

  const handleLogin = () => {
    if (true) {
      return history.push("/manage-customers");
    }
  };

  return (
    <LoginLayout>
      <div className="login__box--right">
        <div className="login__form">
          {/* Logo */}
          <div className="row justify-center">
            <img src={logo} className="mb-30" width="80" alt="EisenVault" />
          </div>

          <div className="login__forms-container">
            {/* The sign-in Form */}
            <section
              className={classnames("login__form--signin", {
                "login__form--signin-hide": !showSignIn,
              })}
            >
              {/* Welcome Message */}
              <div className="row mb-30 login__text-welcome">
                Login to Master Console
              </div>

              {/* Form Elements */}
              <div className="col">
                <input
                  type="email"
                  placeholder="Email Address"
                  id="email"
                  autoComplete="off"
                  className="input-xxl"
                />
              </div>
              <div className="col my-20">
                <input type="password" placeholder="Password" id="password" className="input-xxl" />
              </div>

              <div className="row">
                <button
                  onClick={handleLogin}
                  className="btn btn--info btn--block btn--xxl"
                >
                  Login
                </button>
              </div>
            </section>
            {/* End Login Form */}

            {/* Forgot Password Form */}
            <section
              className={classnames("login__form--forgot-password", {
                "login__form--forgot-password-show": showForgotPassword,
              })}
            >
              {/* Welcome Message */}
              <div className="row mb-20 login__text-welcome justify-center">
                Reset Password
              </div>

              <p className="text-md text-muted mb-30">
                To reset your password, enter the email address you normally use
                to login to EisenVault.
              </p>

              {/* Form Elements */}
              <div className="col">
                <input
                  type="email"
                  placeholder="Email Address"
                  id="email_forgot_password"
                />
              </div>
              <div className="row my-20">
                <button className="btn btn--info btn--block">
                  Retrieve Password
                </button>
              </div>

              {/* Remember me and Forgot Password */}
              <div className="row justify-between align-center">
                <Link
                  to="#"
                  onClick={() => {
                    setShowSignIn(true);
                    setShowForgotPassword(false);
                  }}
                  id="backToLoginBtn"
                  className="login__link text-md"
                >
                  &larr; Back to Login
                </Link>
              </div>
            </section>
            {/* End Forgot Password Form */}
          </div>
        </div>
      </div>
    </LoginLayout>
  );
};

export default Login;
