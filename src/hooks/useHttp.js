import { useState } from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";

const useHttp = url => {
  const history = useHistory();
  const [response, setResponse] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const [error, setError] = useState(null);
  const [postData, setPostData] = useState({});

  async function get(params = {}) {
    try {
      setIsLoading(true);
      const res = await axios.get(url, {
        params
      });
      setResponse(res);
    } catch (error) {
      setHasError(true);
      if (error?.response?.data?.errors) {
        setError(error.response.data.errors);
      }
    } finally {
      setIsLoading(false);
    }
  }

  async function post(postData) {
    try {
      setIsLoading(true);
      const headers = {};
      const response = await axios.post(url, postData, {
        headers
      });
      setResponse(response);
    } catch (error) {
      setHasError(true);

      // If its an unauthorized request, redirect to login page
      if (error?.response?.status === 401) {
        history.push("/");
      }

      if (error?.response?.data?.errors) {
        setError(error.response.data.errors);
      }
    } finally {
      setIsLoading(false);
    }
  }

  async function deletePost(id) {
    try {
      setIsLoading(true);
      const response = await axios.delete(url+`/${id}`);
      setResponse(response);
    } catch (error) {
      setHasError(true);
      if (error?.response?.data?.errors) {
        setError(error.response.data.errors);
      }
    } finally {
      setIsLoading(false);
    }
  }

  return {
    get,
    post,
    deletePost,
    postData,
    setPostData,
    response,
    isLoading,
    hasError,
    error
  };
};

useHttp.propTypes = {
  url: PropTypes.string.isRequired
};

export default useHttp;