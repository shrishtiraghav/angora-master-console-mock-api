import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

// Containers
import LoginContainer from "containers/auth/login/login.container";
import CustomersContainer from "containers/customers/customers.container";
import CustomerUsersContainer from "./containers/customer-users/customer-users.container"
import AccountContainer from "containers/users/user-account/user-account.container";
import AccountUsageContainer from "containers/account-usage/account-usage.container";
import AddTagsContainer from "./containers/manage-tags/add-tags/add-tags.container";
import NewsNotificationContainer from "./containers/news-notifications/news-notifications.container";

const App = () => {
    return (
      <Router>
        <Route exact path="/" component={LoginContainer} />
        <Route exact path="/manage-customers" component={CustomersContainer} />
        <Route exact path="/manage-tags" component={AddTagsContainer}/> 
        <Route path="/manage-customers/:id/users" component={CustomerUsersContainer} />
        <Route path="/manage-customers/:id/profile" component={AccountContainer} />
        <Route path="/manage-customers/:id/contact-persons" component={AccountContainer} />
        <Route path="/manage-customers/:id/subscription-history" component={AccountContainer} />
        <Route path="/manage-customers/:id/account-usage-users" component={AccountUsageContainer} />
        <Route path="/manage-customers/:id/account-usage-activities" component={AccountUsageContainer} />
        <Route path="/manage-customers/news-notifications" component={NewsNotificationContainer} />
      </Router>
    );
  };
  
  export default App;
  