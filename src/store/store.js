import { applyMiddleware, createStore, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import customersReducer from "./customers/customers.reducers";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  customersReducer
});

const middlewareEnhancer = applyMiddleware(thunk);
const composedEnhancers = composeWithDevTools(middlewareEnhancer);

export const store = createStore(rootReducer, composedEnhancers);

export const accessCustomers = state => {
  return state.customersReducer.customers;
};

export const accessCustomerById = state => {
  console.log(state.customersReducer.customer)
  return state.customersReducer.customer;
};

export const accessCustomerPlanAndBilling = state => {
  console.log(state.customersReducer.customersPlanAndBilling)
  return state.customersReducer.customersPlanAndBilling;
};

export const accessCustomerPlanAndBillingById = state => {
  console.log(state.customersReducer.customersPlanAndBilling)
  return state.customersReducer.customerPlanAndBilling;
};
