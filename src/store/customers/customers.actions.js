import http from "helpers/http/http.helper";

export const CUSTOMERS_LOADING = 'CUSTOMERS_LOADING' 
export const GET_CUSTOMERS_SUCCESS = 'GET_CUSTOMERS_SUCCESS'
export const GET_CUSTOMERS_FAILED =  'GET_CUSTOMERS_FAILED'
export const CUSTOMER_POSTING = 'CUSTOMERS_LOADING' 
export const POST_CUSTOMER_SUCCESS = 'GET_CUSTOMERS_SUCCESS'
export const POST_CUSTOMER_FAILED =  'GET_CUSTOMERS_FAILED'
export const CUSTOMER_LOADING = 'CUSTOMER_LOADING' 
export const GET_CUSTOMER_SUCCESS = 'GET_CUSTOMER_SUCCESS'
export const GET_CUSTOMER_FAILED =  'GET_CUSTOMER_FAILED'
export const CUSTOMERS_PLAN_AND_BILLING_LOADING = 'CUSTOMERS_PLAN_AND_BILLING_LOADING' 
export const GET_CUSTOMERS_PLAN_AND_BILLING_SUCCESS = 'GET_CUSTOMERS_PLAN_AND_BILLING_SUCCESS'
export const GET_CUSTOMERS_PLAN_AND_BILLING_FAILED =  'GET_CUSTOMERS_PLAN_AND_BILLING_FAILED'
export const CUSTOMER_PLAN_AND_BILLING_LOADING = 'CUSTOMER_PLAN_AND_BILLING_LOADING' 
export const GET_CUSTOMER_PLAN_AND_BILLING_SUCCESS = 'GET_CUSTOMER_PLAN_AND_BILLING_SUCCESS'
export const GET_CUSTOMER_PLAN_AND_BILLING_FAILED =  'GET_CUSTOMER_PLAN_AND_BILLING_FAILED'

const Customers = '/customers';

export const getCustomers = () => async dispatch => {
    try {
      dispatch({
        type: CUSTOMERS_LOADING,
        payload: true
      });
      const response = await http().get(Customers); 
      const data = response.data
      dispatch({
        type: GET_CUSTOMERS_SUCCESS,
        payload: data.customers
      });
    } catch (error) {
      dispatch({
        type: GET_CUSTOMERS_FAILED,
        payload: error?.response?.data?.errors || error
      });
    } finally {
      dispatch({
        type: CUSTOMERS_LOADING,
        payload: false
      });
    }
  };

  export const postCustomers = () => async dispatch => {
    try {
      dispatch({
        type: CUSTOMER_POSTING,
        payload: true
      });
      const res = await http().post(Customers); 
      const response = await http().get(Customers)
      const data = response.data
      dispatch({
        type: POST_CUSTOMER_SUCCESS,
        payload: data.customers
      });
    } catch (error) {
      dispatch({
        type: POST_CUSTOMER_FAILED,
        payload: error?.response?.data?.errors || error
      });
    } finally {
      dispatch({
        type: CUSTOMER_POSTING,
        payload: false
      });
    }
  };

  export const getCustomerById = ( id ) => async dispatch => {
        try {
          dispatch({
            type: CUSTOMER_LOADING,
            payload: true
          });
            const response = await http().get(`/customer/${id}`);
            dispatch({
                type: GET_CUSTOMER_SUCCESS,
                payload: response.data
            })
        } catch (error) {
            dispatch({
                type: GET_CUSTOMER_FAILED,
                payload: error
            })
        }finally {
          dispatch({
            type: CUSTOMER_LOADING,
            payload: false
          });
        }
    }

    export const getCustomerPlanAndBilling = ( ) => async dispatch => {
      try {
        dispatch({
          type: CUSTOMERS_PLAN_AND_BILLING_LOADING,
          payload: true
        });
          const response = await http().get(`/customerPlanAndBillings`);
          dispatch({
              type: GET_CUSTOMERS_PLAN_AND_BILLING_SUCCESS,
              payload: response.data.customerPlanAndBillings
          })
      } catch (error) {
          dispatch({
              type: GET_CUSTOMERS_PLAN_AND_BILLING_FAILED,
              payload: error
          })
      }finally {
        dispatch({
          type: CUSTOMERS_PLAN_AND_BILLING_LOADING,
          payload: false
        });
      }
  }

  export const getCustomerPlanAndBillingById = ( id ) => async dispatch => {
    try {
      dispatch({
        type: CUSTOMER_PLAN_AND_BILLING_LOADING,
        payload: true
      });
        const response = await http().get(`/customerPlanAndBillings/${id}`);
        dispatch({
            type: GET_CUSTOMER_PLAN_AND_BILLING_SUCCESS,
            payload: response.data
        })
    } catch (error) {
        dispatch({
            type: GET_CUSTOMER_PLAN_AND_BILLING_FAILED,
            payload: error
        })
    }finally {
      dispatch({
        type: CUSTOMER_PLAN_AND_BILLING_LOADING,
        payload: false
      });
    }
}

