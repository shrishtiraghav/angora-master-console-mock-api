import {
    CUSTOMERS_LOADING,
    GET_CUSTOMERS_SUCCESS,
    GET_CUSTOMERS_FAILED,
    CUSTOMER_LOADING,
    GET_CUSTOMER_SUCCESS,
    GET_CUSTOMER_FAILED,
    CUSTOMER_POSTING,
    POST_CUSTOMER_SUCCESS,
    POST_CUSTOMER_FAILED,
    CUSTOMER_PLAN_AND_BILLING_LOADING,
    GET_CUSTOMER_PLAN_AND_BILLING_SUCCESS,
    GET_CUSTOMER_PLAN_AND_BILLING_FAILED,
    CUSTOMERS_PLAN_AND_BILLING_LOADING,
    GET_CUSTOMERS_PLAN_AND_BILLING_SUCCESS,
    GET_CUSTOMERS_PLAN_AND_BILLING_FAILED
  } from "./customers.actions";

  const defaultState = {
    customersIsLoading: false,
    errorLoadingCustomers: null,
    customers: [],
    customerIsLoading: false,
    errorLoadingCustomer: null,
    customer: [],
    customersPlanAndBillingIsLoading: false,
    errorLoadingCustomersPlanAndBilling: null,
    customersPlanAndBilling: [],
    customerPlanAndBillingIsLoading: false,
    errorLoadingCustomerPlanAndBilling: null,
    customerPlanAndBilling: []
  };
  
  const customersReducers = (state = defaultState, { type, payload }) => {
    switch (type) {
      case CUSTOMERS_LOADING:
        return { ...state, customersIsLoading: true };
  
      case GET_CUSTOMERS_SUCCESS:
        return { ...state, customersIsLoading: false, customers: payload };
  
      case GET_CUSTOMERS_FAILED:
        return { ...state, customersIsLoading: false, customers: payload };

      case CUSTOMER_LOADING:
        return { ...state, customerIsLoading: true };
    
      case GET_CUSTOMER_SUCCESS:
        return { ...state, customerIsLoading: false, customer: payload };
    
      case GET_CUSTOMER_FAILED:
        return { ...state, customerIsLoading: false, customer: payload };

      case CUSTOMER_POSTING:
        return { ...state, customersIsLoading: true };
      
      case POST_CUSTOMER_SUCCESS:
        return { ...state, customersIsLoading: false, customers: payload };
      
      case POST_CUSTOMER_FAILED:
        return { ...state, customersIsLoading: false, customers: payload };

      case CUSTOMERS_PLAN_AND_BILLING_LOADING:
        return { ...state, customersPlanAndBillingIsLoading: true };
      
      case GET_CUSTOMERS_PLAN_AND_BILLING_SUCCESS:
        return { ...state, customersPlanAndBillingIsLoading: false, customersPlanAndBilling: payload };
      
      case GET_CUSTOMERS_PLAN_AND_BILLING_FAILED:
        return { ...state, customersPlanAndBillingIsLoading: false, customersPlanAndBilling: payload };  

      case CUSTOMER_PLAN_AND_BILLING_LOADING:
        return { ...state, customerPlanAndBillingIsLoading: true };
        
      case GET_CUSTOMER_PLAN_AND_BILLING_SUCCESS:
        return { ...state, customerPlanAndBillingIsLoading: false, customerPlanAndBilling: payload };
        
      case GET_CUSTOMER_PLAN_AND_BILLING_FAILED:
        return { ...state, customerPlanAndBillingIsLoading: false, customerPlanAndBilling: payload };  
  
      default:
        return state;
    }
  };
  
  export default customersReducers;