// src/server.js
import { createServer, Model ,  belongsTo} from "miragejs";

export function makeServer({ environment = "development" } = {}) {
  let server = createServer({
    environment,

    models: {
      customer: Model.extend({
        customerPlanAndBilling: belongsTo(),
      }),
      tag: Model,
      customerPlanAndBilling: Model.extend({
        customer: belongsTo(),
      })
    },

    seeds(server) {
      server.create("customer", {
        customer_id: "1",
        companyFullLegalName: "Avanta",
        companyDisplayName: "Avanta",
        customerSubdomain: "avanta.eisenvault.cloud",
        membershipType: "premium",
        licenseExpiry: "2022-08-20",
        primaryAdminEmail: "example@gmail.com",
        primaryAdminFirstName: "John",
        primaryAdminLastName: "Doe",
        defaultTimeZone: "",
        defaultDateFormat: "",
        defaultLanguage: "English",
        contactPersons: [
          {
          name: "Joe Montangana",
          contactNumber: "9998884181",
          contactEmail: "joe@gmail.com",
          type: "primary"
        },
        {
          name: "Adam West",
          contactNumber: "9998884181",
          contactEmail: "adam@gmail.com"
        }
      ],
        tags: ["item" , "contact"]
      });
      server.create("customer", {
        companyFullLegalName: "Acme",
        companyDisplayName: "Acme",
        customerSubdomain: "acme.eisenvault.cloud",
        membershipType: "premium",
        licenseExpiry: "2022-08-20",
        primaryAdminEmail: "example@gmail.com",
        primaryAdminFirstName: "John",
        primaryAdminLastName: "Doe",
        defaultTimeZone: "",
        defaultDateFormat: "",
        defaultLanguage: "English",
        contactPersons: [
          {
          name: "Joe Montangana",
          contactNumber: "9998884181",
          contactEmail: "joe@gmail.com",
          type: "primary"
        },
        {
          name: "Adam West",
          contactNumber: "9998884181",
          contactEmail: "adam@gmail.com"
        }
      ],
        tags: ["item" , "contact"]
      });
      server.create("tag", {
        name: "content"
      });
      server.create("tag", {
        name: "item"
      });
      server.create("tag", {
        name: "item2"
      });
      server.create("customerPlanAndBilling" , {
        customer_id: "1",
        deploymentType: "cloud",
        subscriptionPlan: "Premium" ,
        currency: "Rupees",
        paymentMode: "Cheque",
        subscriptionStatus: "Active",
        pricingAndSubscriptionPeriods: [{
          startDate: "2018-08-20",
          endDate: "2027-05-18",
          increment: "10",
          amount: "30000"
        },
        {
          startDate: "2019-08-20",
          endDate: "2027-05-18",
          increment: "10",
          amount: "50000"
        }]
      });
      server.create("customerPlanAndBilling" , {
        customer_id: "2",
        deploymentType: "cloud",
        subscriptionPlan: "Premium" ,
        currency: "Rupees",
        paymentMode: "Cheque",
        subscriptionStatus: "NotActive",
        pricingAndSubscriptionPeriods: {
          startDate: "2018-08-20",
          endDate: "2027-05-18",
          increment: "10",
          amount: "50000"
        }
      })
    },

    routes() {
      this.passthrough()

      this.get("/customers", schema => {
        let customers = schema.customers.all();

        return customers;
      });

      this.get("/customer/:id", (schema, request) => {
        let id = request.params.id
      
        return schema.customers.find(id)
      })
      
      this.post("/customers", (schema, request) => {
        let attrs = JSON.parse(request.requestBody)

        return schema.customers.create(attrs)
      });

      this.patch("/customers/:id", (schema, request) => {
        let newAttrs = JSON.parse(request.requestBody)
        let id = request.params.id
        let movie = schema.movies.find(id)

        return movie.update(newAttrs)
      });

      this.get("/tags", schema => {
        let tags = schema.tags.all();

        return tags;
      });

      
      this.post("/tags", (schema, request) => {
        let attrs = JSON.parse(request.requestBody)

        return schema.tags.create(attrs)
      });

      this.delete("/tags/:id", (schema, request) => {
        let id = request.params.id

        return schema.tags.find(id).destroy()
      });

      this.get("/customerPlanAndBillings" , (schema , request) => {
        let customerPlanAndBilling = schema.customerPlanAndBillings.all();

        return customerPlanAndBilling;
      })
      this.get("/customerPlanAndBillings/:id", (schema, request) => {
        let id = request.params.id
      
        return schema.customerPlanAndBillings.find(id)
      })
    }
  });

  return server;
}
