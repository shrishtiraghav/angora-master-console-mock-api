import axios from "axios";
import PropTypes from "prop-types";

const http = () => {
  let locale = window.localStorage.getItem("EV_LOCALE");
  let token = window.localStorage.getItem("EV_TOKEN");

  if (locale !== null) {
    locale = JSON.parse(locale);
  }

  let tokenHeaders = {};
  if (token !== null) {
    tokenHeaders = {
      authorization: JSON.parse(token)
    };
  }

  const http = axios.create({
    // baseURL: `${window.apiHostname}/api`,
    baseURL: ``,
    timeout: 10000,
    headers: {
      "Accept-Language": locale?.data?.code || "",
      "Content-Type": "application/json",
      "x-portal": "web",
      ...tokenHeaders
    }
  });

  return http;
};

http.propTypes = {
  service: PropTypes.string.isRequired
};

export default http;

