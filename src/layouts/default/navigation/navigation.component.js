import React from "react";

import NavigationLogo from "./navigation-logo/navigation-logo.component";
import NagivationItem from "./navigation-item/navigation-item.component";

// Styles
import "./navigation.styles.scss";

function Navigation() {
  return (
    <aside className="app__navigation">
      <div className="app__navigation--icons-wrapper">
        <div className="section-top">
          {/* Navigation Logo */}
          <NavigationLogo />

          <div className="app__navigation--links-container mt-20">
            {/* Customers */}
            <NagivationItem
            to="/manage-customers"
            iconClassName="fas fa-users"
            tip="customers"
          />

            {/* Manage Tags */}
            <NagivationItem
              to="/manage-tags"
              iconClassName="fas fa-tags"
              tip="Manage Tags"
            />

          </div>
        </div>
      </div>
    </aside>
  );
}

export default Navigation;
