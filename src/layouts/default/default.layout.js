import React, { useState } from "react";

import Header from "./header/header.component";
import Navigation from "./navigation/navigation.component";

// Styles
import "./default.styles.scss";

function Layout({ children }) {

  return (
    <>
      <div className="app__wrapper">
        {/* Header */}
        <Header />

        {/* Left Navigation Area */}
        <Navigation />

        {/* Right Content / Middle Content  Area */}
        <section className="app__content">
          {/* Middle Content */}
          {children}
        </section>
      </div>
    </>
  );
}

export default Layout;
