import React, { useState, useEffect } from "react";

import IconAvatar from "components/icons/icon-avatar/icon-avatar.component";
import ProfileMenu from "./profile-menu/profile-menu.component";

// Styles
import "./profile.styles.scss";

const Profile = () => {
  const [showMenu, setShowMenu] = useState(false);

  // Window event listener
  useEffect(() => {
    function closeMenu(e) {
      if (!e.target.classList.contains("avatar--md")) {
        setShowMenu(false);
      }
    }
    window.addEventListener("click", closeMenu);

    return () => window.removeEventListener("click", closeMenu);
  }, []);

  const handleMenuToggle = () => {
    setShowMenu((prevState) => !prevState);
  };

  return (
    <div className="app__header--user-avatar mr-20">
      <IconAvatar
        source="https://randomuser.me/api/portraits/men/46.jpg"
        alt="Binod Kumar"
        size="md"
        handleOnClick={handleMenuToggle}
      />

      {/* Profile Menu  */}
      <ProfileMenu showMenu={showMenu} />
    </div>
  );
};

export default Profile;
