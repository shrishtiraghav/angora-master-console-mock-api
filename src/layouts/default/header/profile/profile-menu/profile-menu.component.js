import React from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";

import IconAvatar from "components/icons/icon-avatar/icon-avatar.component";

// Styles
import "./profile-menu.styles.scss";

const ProfileMenu = ({ showMenu }) => {
  return (
    <div
      className={classnames("app__profile--menu", {
        hide: !showMenu,
      })}
    >
      <div className="container1">
        <div className="app__profile--header-container">
          <div className="app__profile--avatar mr-10">
            <IconAvatar
              source="https://randomuser.me/api/portraits/men/46.jpg"
              alt="Binod Kumar"
              size="md"
            />
          </div>
          <div className="app__profile--header-right">
            <div className="app__profile--header-name">Binod Kumar</div>
            <div className="app__profile--header-email">
              binod.kumar@eisenvault.cloud
            </div>
          </div>
        </div>

        <div className="hr">&nbsp;</div>

        <ul className="app__profile-menu-list">
          <li>
            <Link to="/users/1/profile">Edit Profile</Link>
          </li>
          <li>
            <Link to="/">Logout</Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default ProfileMenu;
