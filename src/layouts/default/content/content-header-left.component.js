import React from "react";

const ContentHeaderLeftLayout = ({ children }) => {
  return (
    <div className="app__data--listing-header-left-col">
      <div className="app__data--listing-title">{children}</div>
    </div>
  );
};

export default ContentHeaderLeftLayout;
