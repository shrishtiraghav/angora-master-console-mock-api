import ContentHeaderMain from "./content-header-main.component";
import ContentHeaderLeft from "./content-header-left.component";
import ContentHeaderRight from "./content-header-right.component";

const ContentHeaderLayout = {};

ContentHeaderLayout.Main = ContentHeaderMain;
ContentHeaderLayout.Left = ContentHeaderLeft;
ContentHeaderLayout.Right = ContentHeaderRight;

export default ContentHeaderLayout;
