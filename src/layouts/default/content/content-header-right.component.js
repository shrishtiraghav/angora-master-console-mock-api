import React from "react";

const ContentHeaderRightLayout = ({ children }) => {
  return <div className="app__data--listing-header-right-col">{children}</div>;
};

export default ContentHeaderRightLayout;
