import React from "react";

const ContentHeaderMainLayout = ({ children }) => {
  return (
    <div className="app__data--listing-header">
      <div className="app__data--listing-header-top mb-20">{children}</div>
    </div>
  );
};

export default ContentHeaderMainLayout;
