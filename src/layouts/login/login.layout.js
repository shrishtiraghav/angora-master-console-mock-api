import React from "react";
import { Link } from "react-router-dom";

// Styles
import "./login.styles.scss"

const Login = ({ children }) => {
  return (
    <div className="login__wrapper">
      {/* Login Form */}
      {children}
    </div>
  );
};

export default Login;